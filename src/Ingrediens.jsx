import { Fragment } from "react";

const Ingredients = ({ingredients}) => {
    return ( 
        <Fragment>
            <ul>
                {ingredients.map((bahan) => (
                <li>{bahan.text}</li>
                ))}
            </ul>
        </Fragment>
     );
}
 
export default Ingredients;