import React from "react";
import "./RecipeDetails.css";

const RecipeDetails = ({ ingredients }) => {
    return (
        <div className="recipe__ingredients">
            <ul>
                {ingredients.map(bahan=>(
                    <li className="ingredient-text">{bahan.text}</li>
                ))}
            </ul>
        </div>
    );
};

export default RecipeDetails;