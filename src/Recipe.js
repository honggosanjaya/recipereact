import React, { useState, Fragment } from "react";
import "./Recipe.css";
import RecipeDetails from "./RecipeDetails";

const Recipe = ({ title, calories, image, source, ingredients}) => {
  const [show, setShow] = useState(false);

  const handleRecipeBtn = () =>{
    setShow(!show)
  }

  return (
    <Fragment>
      <div className="recipe__card">
        <img src={image} alt="foods" />
        <h1 className="recipe__name">{title}</h1>
        <h2 className="recipe__source">Source: {source}</h2>
        <p>Calories: {calories}</p>
          <button onClick={handleRecipeBtn} type="button" className="btn btn-sm recipe__btn">
            Get Recipe
          </button>
          {show && <RecipeDetails ingredients={ingredients} />}
      </div>
      
    </Fragment>

    
  );
};

export default Recipe;
