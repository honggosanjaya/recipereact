import React, { useEffect, useState } from "react";
import "./HomePage.css";
import Recipe from "./Recipe";
import chicken from './images/chicken.png';
import pizza from './images/pizza.png';
import peanut from './images/peanut.png';
import juice from './images/juice.png'

const HomePage = () => {
  const APP_ID = "0bc9ce1c";
  const APP_KEY = "308fb28dde0da109b6b977efc7acecaa";

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState("chicken");
  
  useEffect(() => {
    const getRecipes = async () => {
      const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`);
      const data = await response.json();
      setRecipes(data.hits);
    };
    getRecipes();
  }, [query]);

  const updateSearch = (e) => {
    setSearch(e.target.value);
  };

  const getSearch = (e) => {
    e.preventDefault();
    setQuery(search);
    setSearch("");
  };

  const handleChickenBtn = () =>{
    setQuery("chicken");
  }

  const handlePizzaBtn = () =>{
    setQuery("pizza");
  }

  const handlePeanutBtn = () =>{
    setQuery("peanut");
  }

  const handleJuiceBtn = () =>{
    setQuery("juice");
  }

  return (
    <div className="App container">
      <header>
        <form className="form-inline">
            <input type="text" className="form-control header__search" placeholder="what are you looking for?" value={search} onChange={updateSearch} />
          <button onClick={getSearch} type="submit" className="search-btn">
              <i className="bi bi-search"></i>
            </button>
        </form>
      </header>

      <div className="popular">
        <div className="row">
          <div className="col-12">
            <h1>Popular Search</h1>
          </div>
        </div>
        <div className="row justify-content-between">
          <div className="col-3">
            <img src={chicken} alt="peanut" onClick={handleChickenBtn}/>
          </div>
          <div className="col-3">
            <img src={pizza} alt="peanut" onClick={handlePizzaBtn}/>
          </div>
          <div className="col-3">
            <img src={peanut} alt="peanut" onClick={handlePeanutBtn}/>
          </div>
          <div className="col-3">
            <img src={juice} alt="peanut" onClick={handleJuiceBtn}/>
          </div>
        </div>
      </div>

      <div className="App__resep">
        {recipes.map((resep) => (
          <Recipe key={resep.recipe.label} title={resep.recipe.label} 
          source={resep.recipe.source} calories={resep.recipe.calories} image={resep.recipe.image} 
          ingredients={resep.recipe.ingredients} query={query} />
        ))}
      </div>
    </div>
  );
};

export default HomePage;
